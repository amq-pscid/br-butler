// ==UserScript==
// @name         BR Butler UI
// @version      1.0.3
// @description  UI version of the BR Butler script.
// @author       PsCid
// @match        https://animemusicquiz.com/*
// @grant        none
// @require      https://raw.githubusercontent.com/TheJoseph98/AMQ-Scripts/master/common/amqWindows.js
// @require      https://raw.githubusercontent.com/TheJoseph98/AMQ-Scripts/master/common/amqScriptInfo.js
// ==/UserScript==



// ----------------------
// Mostrar nomes no mapa = Shift + 'P'
// Apagar nomes no mapa = Shift + 'O'
// ----------------------


if (document.getElementById("startPage")) return;

// Main
$(document.documentElement).keydown(function (event) {
    if (document.querySelector("#mhShowSelectionSlider > div.slider-handle.min-slider-handle.round").getAttribute("aria-valuenow") == 2) {
        var list;
        var i = 0;
        if (event.which === 80 && event.shiftKey === true) {
            list = document.querySelectorAll(".brShowEntry.brMapObject");
            for(i = 0; i < list.length; ++i) {
                simulateMouseClick(list[i], 'on');
            }
        } else if (event.which === 79 && event.shiftKey === true) {
            list = document.querySelectorAll(".brShowEntry.brMapObject");
            for(i = 0; i < list.length; ++i) {
                simulateMouseClick(list[i], 'off');
            }
        }

    }
});


// Mouse simulation
function simulateMouseClick(targetNode, type) {
    function triggerMouseEvent(targetNode, eventType) {
        var clickEvent = document.createEvent('MouseEvents');
        clickEvent.initEvent(eventType, true, true);
        targetNode.dispatchEvent(clickEvent);
    }
    if (type == 'on') {
        ["mouseover"].forEach(function(eventType) {
            triggerMouseEvent(targetNode, eventType);
        });
    } else {
        ["mouseout"].forEach(function(eventType) {
            triggerMouseEvent(targetNode, eventType);
        });
    }
}


// BR Picks
var pickList;
var pickAlreadyPlayed = "❌";
function saveAndPrintBrPicks() {
    var currentIndex = 1;
    pickList = document.querySelectorAll("#brCollectedList li");
    if (pickList.length >= 1) {
        clearTable();
        deleteLocalStorage();
        for (var i = 0; i < pickList.length; ++i) {
            localStorage.setItem('pick ' + currentIndex, pickList[i].textContent.substring(2, pickList[i].textContent.length));
            pickWindowTable.append(addTableEntry(currentIndex, ""));
            localStorage.setItem('length', currentIndex.toString());
            currentIndex += 1;
        }
    }
}

function writePickToField(animeIndex, element) {
    if (localStorage.getItem('pick ' + animeIndex) !== null) {
        var input = document.getElementById(element);
        var pickName = localStorage.getItem('pick ' + animeIndex).replace(pickAlreadyPlayed,"");
        input.value = pickName;
        var ev = document.createEvent('Event');
        ev.initEvent('keypress');
        ev.which = ev.keyCode = 13;
        input.dispatchEvent(ev);
    }
}

async function wait() {
    await new Promise(resolve => setTimeout(resolve, 10));
}

function checkPicks(newSong) {
    if (localStorage.getItem('length') != null) {
        clearTable();
        for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
            if ((localStorage.getItem('pick ' + i).replace(pickAlreadyPlayed,"") == newSong.anime.english || localStorage.getItem('pick ' + i).replace(pickAlreadyPlayed,"") == newSong.anime.romaji) || localStorage.getItem('pick ' + i).endsWith(pickAlreadyPlayed)) {
                pickWindowTable.append(addTableEntry(i, "✔️"));
                localStorage.setItem('pick ' + i, localStorage.getItem('pick ' + i).replace(pickAlreadyPlayed,"") + pickAlreadyPlayed);
            } else {
                pickWindowTable.append(addTableEntry(i, ""));
            }
        }
    }
}

function sendPicksToChat() {
    for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
        writePickToField(i, "gcInput");
    }
}

function copyPicks() {
    var pickString = "";
    for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
        if (i > 1) {
            pickString = pickString + " | ";
        }
        pickString = pickString + localStorage.getItem('pick ' + i).replace(pickAlreadyPlayed,"");
    }

    const listener = function(ev) {
        ev.preventDefault();
        ev.clipboardData.setData('text/plain', pickString);
    };

    document.addEventListener('copy', listener);
    document.execCommand('copy');
    document.removeEventListener('copy', listener);

    gameChat.systemMessage("Lista de picks copiada ✔️");
}

// UI
let pickWindow;
let pickWindowTable;
let pickWindowOpenButton;

function createPickWindow() {
    pickWindow = new AMQWindow({
        title: "BR Picks",
        width: 360,
        height: 260,
        minWidth: 180,
        minHeight: 170,
        zIndex: 1060,
        resizable: true,
        draggable: true
    });

    pickWindow.addPanel({
        id: "pickWindowOptions",
        width: 1.0,
        height: 65
    });

    pickWindow.addPanel({
        id: "pickWindowTableContainer",
        width: 1.0,
        height: "calc(100% - 65px)",
        position: {
            x: 0,
            y: 65
        },
        scrollable: {
            x: true,
            y: true
        }
    });

    pickWindow.panels[0].panel
        .append($(`<button class="btn btn-default pickListOptionsButton" type="button"><i aria-hidden="true" class="fa fa-clipboard"></i></button>`)
                .click(() => {
        copyPicks();
    })
                .popover({
        placement: "bottom",
        content: "Copiar",
        trigger: "hover",
        container: "body",
        animation: false
    })
               )
        .append($(`<button class="btn btn-default pickListOptionsButton" type="button"><i aria-hidden="true" class="fa fa-share-square-o"></i></button>`)
                .click(() => {
        sendPicksToChat();
    })
                .popover({
        placement: "bottom",
        content: "Enviar",
        trigger: "hover",
        container: "body",
        animation: false
    })
               );

    pickWindowTable = $(`<table id="pickWindowTable" class="table floatingContainer"></table>`);
    pickWindow.panels[1].panel.append(pickWindowTable);

    clearTable();

    pickWindowOpenButton = $(`<div id="qpPickListButton" class="clickAble qpOption"><i aria-hidden="true" class="fa fa-list-alt qpMenuItem"></i></div>`)
        .click(function () {
        if(pickWindow.isVisible()) {
            $(".rowSelected").removeClass("rowSelected");
            pickWindow.close();
        }
        else {
            pickWindow.open();
        }
    })
        .popover({
        placement: "bottom",
        content: "BR Picks",
        trigger: "hover"
    });

    let oldWidth = $("#qpOptionContainer").width();
    $("#qpOptionContainer").width(oldWidth + 35);
    $("#qpOptionContainer > div").append(pickWindowOpenButton);

    pickWindow.body.attr("id", "pickWindowBody");
}

function clearTable() {
    pickWindowTable.children().remove();

    let header = $(`<tr class="pickHeader"></tr>`)
    let nameCol = $(`<td class="brPickName"><b>Anime<b></td>`);
    let appearedCol = $(`<td class="brPickAppeared"><b>Saiu</b></td>`);

    header.append(nameCol);
    header.append(appearedCol);
    pickWindowTable.append(header);
}

function addTableEntry(rowIndex, appeared) {
    let newRow = $(`<tr class="pickData clickAble"></tr>`)
    .click(function () {
        if (!$(this).hasClass("rowSelected")) {
            $(".rowSelected").removeClass("rowSelected");
            $(this).addClass("rowSelected");
            writePickToField(rowIndex, "qpAnswerInput");
        }
        else {
            $(".rowSelected").removeClass("rowSelected");
        }
    })
    .hover(function () {
        $(this).addClass("hover");
    }, function () {
        $(this).removeClass("hover");
    })

    var name = localStorage.getItem('pick ' + rowIndex).replace(pickAlreadyPlayed,"");
    let nameCol = $(`<td class="brPickName"></td>`).text(name);
    let appearedCol = $(`<td class="brPickAppeared"></td>`).text(appeared);

    newRow.append(nameCol);
    newRow.append(appearedCol);
    return newRow;
}

function deleteLocalStorage() {
    for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
        localStorage.removeItem('pick ' + i);
    }
    localStorage.removeItem('length');
}


// Setup
let loadInterval = setInterval(() => {
    if (document.getElementById("loadingScreen").classList.contains("hidden")) {
        setup();
        clearInterval(loadInterval);
    }
}, 500);

function setup() {
    let resultsListener = new Listener("answer results", (result) => {
        let newSong = {
            anime: result.songInfo.animeNames
        };
        checkPicks(newSong);
    });

    let quizReadyListener = new Listener("quiz ready", (data) => {
        saveAndPrintBrPicks();
    });

    let quizOverListener = new Listener("quiz over", (roomSettings) => {
        clearTable();
        deleteLocalStorage();
        if (pickWindow.isVisible()) {
            pickWindow.close();
        }
    });

    let quizReturnToLobbyListener = new Listener("return lobby vote result", (payload) => {
        if (payload.passed) {
            clearTable();
            deleteLocalStorage();
            if (pickWindow.isVisible()) {
                pickWindow.close();
            }
        }
    });

    let quizLeaveListener = new Listener("New Rooms", (rooms) => {
        clearTable();
        deleteLocalStorage();
        if (pickWindow.isVisible()) {
            pickWindow.close();
        }
    });

    createPickWindow();

    resultsListener.bindListener();
    quizReadyListener.bindListener();
    quizOverListener.bindListener();
    quizLeaveListener.bindListener();
    quizReturnToLobbyListener.bindListener();

    $(".modal").on("show.bs.modal", () => {
        pickWindow.setZIndex(1030);
    });

    $(".modal").on("hidden.bs.modal", () => {
        pickWindow.setZIndex(1060);
    });

    $(".slCheckbox label").hover(() => {
        pickWindow.setZIndex(1030);
    }, () => {
        pickWindow.setZIndex(1060);
    });

    AMQ_addStyle(`
        #pickWindowTableContainer {
            padding: 15px;
        }
        #qpPickListButton {
            width: 30px;
            height: 100%;
            margin-right: 5px;
        }
        .brPickName {
            min-width: 90px;
        }
        .brPickAppeared {
            min-width: 20px;
        }
        .pickListOptionsButton {
            float: left;
            margin-top: 15px;
            margin-left: 10px;
            padding: 6px 8px;
        }
        .standard .pickData {
            height: 50px;
        }
        .pickHeader > td {
            border: 1px solid black;
            text-align: center;
            vertical-align: middle;
        }
        .compact .pickHeader > td {
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }
        .pickData > td {
            vertical-align: middle;
            border: 1px solid black;
            text-align: center;
        }
        .pickData.guessHidden {
            background-color: rgba(0, 0, 0, 0);
        }
        .pickData.hover {
            box-shadow: 0px 0px 10px cyan;
        }
        .pickData.rowSelected {
            box-shadow: 0px 0px 10px lime;
        }
        .compact .pickData {
            height: 20px;
        }
        .compact .pickData > td {
            vertical-align: middle;
            border: 1px solid black;
            text-align: center;
            text-overflow: ellipsis;
            overflow: hidden;
            padding: 0px 5px;
            white-space: nowrap;
            font-size: 14px;
            line-height: 1;
        }
    `);
}