// ==UserScript==
// @name         BR Butler UI
// @version      2.0.13
// @description  Save BR picks and more.
// @author       PsCid
// @match        https://animemusicquiz.com/*
// @grant        none
// ==/UserScript==


// ----------------------
// Mostrar nomes no mapa = Shift + 'P'
// Apagar nomes no mapa = Shift + 'O'
// ----------------------


if (document.getElementById("startPage")) return;

// Main
$(document.documentElement).keydown(function (event) {
    if (document.querySelector("#mhShowSelectionSlider > div.slider-handle.min-slider-handle.round").getAttribute("aria-valuenow") == 2) {
        var list;
        var i = 0;
        if (event.which === 80 && event.shiftKey === true) {
            list = document.querySelectorAll(".brShowEntry.brMapObject");
            for(i = 0; i < list.length; ++i) {
                simulateMouseClick(list[i], 'on');
            }
        } else if (event.which === 79 && event.shiftKey === true) {
            list = document.querySelectorAll(".brShowEntry.brMapObject");
            for(i = 0; i < list.length; ++i) {
                simulateMouseClick(list[i], 'off');
            }
        }
    }
});

var brChatTemplate = `<div id="brMsgBox" class="chatBox">
    <div class="chatBoxContainer">
        <div class="chatTopBar">
            <p>BR Picks</p>
            <span id="brMsgBoxCloseBtn" class="glyphicon glyphicon-remove clickAble" aria-hidden="true"></span>
            <span id="brMsgBoxCopyBtn" class="glyphicon clickAble playerProfile" aria-hidden="true"><i class="fa fa-clipboard" aria-hidden="true"></i></span>
            <span id="brMsgBoxSendBtn" class="glyphicon clickAble playerProfile" aria-hidden="true"><i class="fa fa-share-square-o" aria-hidden="true"></i></span>
        </div>
        <ul id="brMsgBoxChatContent" class="chatContent"></ul>
    </div>
    <div id="brMsgBoxFooterBtn" class="chatBoxFooter text-center clickAble leftRightButtonBottom">
        <h4 id="brMsgBoxFooterHigh1" class="chatHighlight"><span>BR Picks</span></h4>
        <h4 id="brMsgBoxFooterHigh2" class="chatHighlight chatBoxTextGlow"><span>BR Picks</span></h4>
    </div>
</div>`;

function createBrChatBox(firstStage) {
    var chatElement =  document.getElementById("brMsgBox");
    if (chatElement == null) {
        var msgBox = $(format(brChatTemplate, "BR Picks"));
        $("#activeChatScrollContainer").append(msgBox);
        chatElement = document.getElementById("brMsgBox");
        var msgBoxContainer = chatElement.getElementsByClassName("chatBoxContainer")[0];
        var chatRollBtn = document.getElementById("brMsgBoxFooterBtn");
        chatRollBtn.addEventListener('click', function () {
            if (msgBoxContainer.style.transform != "translateY(0%)") {
                msgBoxContainer.style.transform = "translateY(0%)";
            } else {
                msgBoxContainer.style.transform = "translateY(105%)";
            }
            document.getElementById("brMsgBoxFooterHigh1").classList.remove("chatHighlight");
            document.getElementById("brMsgBoxFooterHigh2").classList.remove("chatHighlight");
        }, false);

        var copyPicksBtn = document.getElementById("brMsgBoxCopyBtn");
        copyPicksBtn.addEventListener('click', function () {
            copyPicks();
        }, false);

        var sendPicksBtn = document.getElementById("brMsgBoxSendBtn");
        sendPicksBtn.addEventListener('click', function () {
            sendPicksToChat();
        }, false);

        var closeBtn = document.getElementById("brMsgBoxCloseBtn");
        closeBtn.addEventListener('click', function () {
            if (chatElement != null) {
                removeBrChat();
            }
        }, false);

        if (firstStage) {
            saveAndPrintBrPicks();
        }
    }
}

function isOdd(num) { return num % 2;}

function addPickToBrChatBox(rowIndex, appeared, correct) {
    var li;
    var ul = document.getElementById("brMsgBoxChatContent");
    if (ul == null) { return false;}
    if (document.getElementById("brBoxPick" + rowIndex) == null) {
        li = document.createElement("li");
        li.setAttribute("id", "brBoxPick" + rowIndex);
        li.classList.add("chatPicksLi");
        colorPick(rowIndex, li, appeared, correct);
        li.appendChild(document.createTextNode(localStorage.getItem('pick ' + rowIndex).replace(pickAlreadyPlayedCorrect,"").replace(pickAlreadyPlayedIncorrect,"")));
        ul.appendChild(li);

        var answerPick = li;
        answerPick.addEventListener('click', function () {
            writePickToField(rowIndex, "qpAnswerInput");
        }, false);
    } else {
        li = document.getElementById("brBoxPick" + rowIndex);
        colorPick(rowIndex, li, appeared, correct);
    }
}

function colorPick(rowIndex, li, appeared, correct) {
    if (appeared) {
        if (correct) {
            li.style.background = "darkgreen";
        } else {
            li.style.background = "darkred";
        }
    } else {
        if (isOdd(rowIndex)) {
            li.style.background = "dimgray";
        }
    }
}

function removeBrChat() {
    if (document.getElementById("brMsgBox") != null) {
        document.getElementById("brMsgBox").remove();
    }
}

// Mouse simulation
function simulateMouseClick(targetNode, type) {
    function triggerMouseEvent(targetNode, eventType) {
        var clickEvent = document.createEvent('MouseEvents');
        clickEvent.initEvent(eventType, true, true);
        targetNode.dispatchEvent(clickEvent);
    }
    if (type == 'on') {
        ["mouseover"].forEach(function(eventType) {
            triggerMouseEvent(targetNode, eventType);
        });
    } else {
        ["mouseout"].forEach(function(eventType) {
            triggerMouseEvent(targetNode, eventType);
        });
    }
}


// BR Picks
var pickList;
var pickAlreadyPlayedCorrect = "✔️";
var pickAlreadyPlayedIncorrect = "❌";
function saveAndPrintBrPicks() {
    var currentIndex = 1;
    pickList = document.querySelectorAll("#brCollectedList li");
    if (pickList.length >= 1) {
        deleteLocalStorage();
        for (var i = 0; i < pickList.length; ++i) {
            localStorage.setItem('pick ' + currentIndex, pickList[i].textContent.substring(2, pickList[i].textContent.length));
            addPickToBrChatBox(currentIndex, false, true);
            localStorage.setItem('length', currentIndex.toString());
            currentIndex += 1;
        }
    }
}

function writePickToField(animeIndex, element) {
    if (localStorage.getItem('pick ' + animeIndex) !== null) {
        var input = document.getElementById(element);
        var pickName = localStorage.getItem('pick ' + animeIndex).replace(pickAlreadyPlayedCorrect,"").replace(pickAlreadyPlayedIncorrect,"");
        input.value = pickName;
        var ev = document.createEvent('Event');
        ev.initEvent('keypress');
        ev.which = ev.keyCode = 13;
        input.dispatchEvent(ev);
    }
}

async function wait() {
    await new Promise(resolve => setTimeout(resolve, 10));
}

function checkPicks(newSong) {
    if (localStorage.getItem('length') != null) {
        for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
            var currentPick = localStorage.getItem('pick ' + i);
            if ((currentPick.replace(pickAlreadyPlayedCorrect,"").replace(pickAlreadyPlayedIncorrect,"") == newSong.anime.english
                 || currentPick.replace(pickAlreadyPlayedCorrect,"").replace(pickAlreadyPlayedIncorrect,"") == newSong.anime.romaji)
                || (currentPick.endsWith(pickAlreadyPlayedCorrect) || currentPick.endsWith(pickAlreadyPlayedIncorrect))) {
                if (currentPick.endsWith(pickAlreadyPlayedCorrect)) {
                    localStorage.setItem('pick ' + i, currentPick.replace(pickAlreadyPlayedCorrect,"") + pickAlreadyPlayedCorrect);
                    addPickToBrChatBox(i, true, true);
                } else if (currentPick.endsWith(pickAlreadyPlayedIncorrect)) {
                    localStorage.setItem('pick ' + i, currentPick.replace(pickAlreadyPlayedIncorrect,"") + pickAlreadyPlayedIncorrect);
                    addPickToBrChatBox(i, true, false);
                } else {
                    if (newSong.correct) {
                        localStorage.setItem('pick ' + i, currentPick.replace(pickAlreadyPlayedCorrect,"") + pickAlreadyPlayedCorrect);
                    } else {
                        localStorage.setItem('pick ' + i, currentPick.replace(pickAlreadyPlayedIncorrect,"") + pickAlreadyPlayedIncorrect);
                    }
                    addPickToBrChatBox(i, true, newSong.correct);
                }
            } else {
                addPickToBrChatBox(i, false, newSong.correct);
            }
        }
    }
}

function sendPicksToChat() {
    for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
        writePickToField(i, "gcInput");
    }
}

function copyPicks() {
    var pickString = "";
    for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
        if (i > 1) {
            pickString = pickString + " | ";
        }
        pickString = pickString + localStorage.getItem('pick ' + i).replace(pickAlreadyPlayedCorrect,"").replace(pickAlreadyPlayedIncorrect,"");
    }

    const listener = function(ev) {
        ev.preventDefault();
        ev.clipboardData.setData('text/plain', pickString);
    };

    document.addEventListener('copy', listener);
    document.execCommand('copy');
    document.removeEventListener('copy', listener);

    if (pickString.length > 150) {
        gameChat.systemMessage("⚠️ Lista muito grande, talvez não seja possível enviar numa única mensagem. Tente o envio direto.");
    } else {
        gameChat.systemMessage("✔️ Picks copiados.");
    }
}

function deleteLocalStorage() {
    for (var i = 1; i <= parseInt(localStorage.getItem('length')); ++i) {
        localStorage.removeItem('pick ' + i);
    }
    localStorage.removeItem('length');
}

// Setup
let loadInterval = setInterval(() => {
    if (document.getElementById("loadingScreen").classList.contains("hidden")) {
        setup();
        clearInterval(loadInterval);
    }
}, 500);

function isBattleRoyale() {
    return document.querySelector("#mhShowSelectionSlider > div.slider-handle.min-slider-handle.round").getAttribute("aria-valuenow") == 2;
}

function setup() {
    let resultsListener = new Listener("answer results", (result) => {
        if (isBattleRoyale()) {
            let newSong = {
                anime: result.songInfo.animeNames
            };
            let findPlayer = Object.values(quiz.players).find((tmpPlayer) => {
                return tmpPlayer._name === selfName && tmpPlayer.avatarSlot._disabled === false
            });
            if (findPlayer !== undefined) {
                let playerIdx = Object.values(result.players).findIndex(tmpPlayer => {
                    return findPlayer.gamePlayerId === tmpPlayer.gamePlayerId
                });
                newSong.correct = result.players[playerIdx].correct;
            }
            createBrChatBox(false);
            checkPicks(newSong);
        }
    });

    let quizReadyListener = new Listener("quiz ready", (data) => {
        if (isBattleRoyale()) {
            createBrChatBox(true);
        }
    });

    let quizOverListener = new Listener("quiz over", (roomSettings) => {
        if (isBattleRoyale()) {
            deleteLocalStorage();
            removeBrChat();
        }
    });

    let quizReturnToLobbyListener = new Listener("return lobby vote result", (payload) => {
        if (payload.passed) {
            if (isBattleRoyale()) {
                deleteLocalStorage();
                removeBrChat();
            }
        }
    });

    let quizLeaveListener = new Listener("New Rooms", (rooms) => {
        if (isBattleRoyale()) {
            deleteLocalStorage();
            removeBrChat();
        }
    });

    resultsListener.bindListener();
    quizReadyListener.bindListener();
    quizOverListener.bindListener();
    quizLeaveListener.bindListener();
    quizReturnToLobbyListener.bindListener();

    addCss(`
        #brMsgBoxChatContent {
            overflow-x: hidden;
            scrollbar-width: none;
            height: 184px;
        }
        #brMsgBoxChatContent::-webkit-scrollbar {
            width: 0px;
        }
        .chatPicksLi {
            border-bottom: 1px solid black;
            padding-left: 1px;
            cursor: pointer;
        }
    `);
}

function addCss(css) {
    let head = document.head;
    let style = document.createElement("style");
    head.appendChild(style);
    style.type = "text/css";
    style.appendChild(document.createTextNode(css));
}